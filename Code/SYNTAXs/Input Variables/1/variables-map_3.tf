variable "lightsabre_color_map" {
  type = map(set(string))
  default = {
    luke = ["green", "blue"]
    yoda = ["green"]
    darth = ["red"]
  }
}
