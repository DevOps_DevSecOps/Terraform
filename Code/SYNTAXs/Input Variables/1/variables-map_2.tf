variable "lightsabre_color_map" {
  type = map(list(string))
  default = {
    luke = ["green", "blue"]
    yoda = ["green"]
    darth = ["red"]
  }
}
