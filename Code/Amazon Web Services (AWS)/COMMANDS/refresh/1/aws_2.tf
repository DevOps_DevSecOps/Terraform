resource "aws_instance" "web" {
  ami           = "ami-0638cf52a5b22be19"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["sg-00c87fc200cda91c6", "sg-0dda58e22882958c9"]
}

resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16"
  tags = {
    Name = "AWS_VPC",
    "ENV"  = "Prod"
    "no"   = "1"
  }
}

resource "aws_s3_bucket" "bucket" {
  bucket = "my-tf-test-bucket-123987546"
  tags = {
    Name        = "S3_Bucket"
  }
}

