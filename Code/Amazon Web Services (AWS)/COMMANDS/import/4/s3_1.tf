resource "aws_s3_bucket" "my_test_bucket" {
  bucket = "test-bucket-1-for-import"
  tags = {
    "name" = "test-bucket"
  }
}

resource "aws_s3_bucket_acl" "example" {
  bucket = aws_s3_bucket.my_test_bucket.id
}
