resource "aws_instance" "ec2_example" {
  ami            = "ami-06ce824c157700cd2"
  instance_type  = "t2.micro"
  tags = {
    "Name" = "my-test-ec2"
  }
}
