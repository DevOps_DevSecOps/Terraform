terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 4.4.0"
        }
    }
    required_version = ">=1.1.0"
}

provider "aws" {
    region = "us-west-2"
    profile = "terraform-user"
}

resource "aws_instance" "demo-instance" {
  ami           = "unknown"
  instance_type = "unknown"
}
