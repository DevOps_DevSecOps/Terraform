terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "ap-south-1"
}


resource "aws_instance" "example" {
  ami           = "ami-0a0f1259dd1c90938"  # Specify the AMI ID for your desired Linux distribution
  instance_type = "t2.micro"  # Change this to your desired instance type
  key_name      = "demo"  # Replace with the name of the key pair created in Step 1

  connection {
    type        = "ssh"
    user        = "ec2-user"  # Default user for Amazon Linux. For other distributions, use the appropriate username (e.g., ubuntu, centos)
    private_key = file("./demo.pem")  # Replace with the path to your downloaded .pem file
    host        = aws_instance.example.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum install -y nginx",
      "sudo yum update -y",
      "sudo amazon-linux-extras intall -y ngnix1",
      "sudo systemctl start nginx"

    ]
    on_failure = continue
  }

  tags = {
    Name = "example"
  }
}
