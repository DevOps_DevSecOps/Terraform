terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "web" {
  ami           = "ami-06195c6348fb0b567"
  instance_type = "t2.micro"
  provisioner "local-exec" {
    command = "echo The server's IP address is ${self.private_ip}"
  }
}
