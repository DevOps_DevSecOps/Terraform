terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.10.0"
    }
  }
}

provider "aws" {
  region  = "us-west-2"
}

resource "aws_instance" "app_server" {
  ami           = "ami-0638cf52a5b22be19"
  instance_type = "t2.micro"
  key_name      = "EC2"

  provisioner "file" {
    source = "script.sh"
    destination = "/home/ubuntu/script.sh"

    connection {
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("/home/azureuser/EC2.pem")}"
      host = "${aws_instance.app_server.public_ip}"
      port = 22
    }
  }

  provisioner "local-exec" {
    command = "echo ${self.private_ip} >> private_ips.txt"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.app_server.public_ip} >> public_ips.txt"
    
    connection {
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("/home/azureuser/EC2.pem")}"
      host = "${aws_instance.app_server.public_ip}"
      port = 22
    }
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update -y",
      "sudo hostname",
      "sudo sh ~/script.sh",
    ]

    connection {
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("/home/azureuser/EC2.pem")}"
      host = "${aws_instance.app_server.public_ip}"
      port = 22
    }
  }

}

