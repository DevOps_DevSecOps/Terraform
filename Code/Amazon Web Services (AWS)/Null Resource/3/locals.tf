locals {
  name         = "App_server"
  project_name = "example_project"
  owner        = "team_name"
}

locals {
  common_tags = {
    Project = local.project_name
    Owner   = local.owner
    Name    = local.name
  }
}
