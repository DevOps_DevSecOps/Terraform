provider "aws" {
  region  = "us-west-2"
}

resource "aws_instance" "app_server" {
  ami           = "ami-0638cf52a5b22be19"
  instance_type = "t2.micro"
  key_name      = "EC2"
  vpc_security_group_ids = ["sg-07abb242d4d86d698"]
  tags = {
    "Name" = "APP"
  }

# Establishes connection to be used by all provisioners (i.e. file/local-exec/remote-exec)
  connection {
    type     = "ssh"
    user     = "ubuntu"
    private_key = "${file("/home/azureuser/EC2.pem")}"
    host = "${aws_instance.app_server.public_ip}"
    port = 22
  }

# file
  provisioner "file" {
    source = "script_1.sh"
#   source = "script_2.sh"
    destination = "/home/ubuntu/script.sh"
  }

# local-exec
  provisioner "local-exec" {
    command = "echo ${self.private_ip} >> private_ips.txt"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.app_server.public_ip} >> public_ips.txt"
  }

# remote-exec
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update -y",
      "sudo hostname"
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "sudo sh ./script.sh"
    ]
  }

}

# Null Resource
resource "null_resource" "example_1" {

# Establishes connection to be used by all provisioners (i.e. file/local-exec)
  connection {
    type     = "ssh"
    user     = "ubuntu"
    private_key = "${file("/home/azureuser/EC2.pem")}"
    host = "${aws_instance.app_server.public_ip}"
    port = 22
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.app_server.private_ip} >> private.txt"
  }

  provisioner "file" {
    source = "script_3.sh"
    destination = "/home/ubuntu/script_3"
  }
}

resource "null_resource" "example_2" {

  provisioner "remote-exec" {
    inline = [ "sudo chmod 777 /home/ubuntu/script_3" ]

# Establishes connection to be used by all provisioners (i.e. remote-exec)
    connection {
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("/home/azureuser/EC2.pem")}"
      host = "${aws_instance.app_server.public_ip}"
      port = 22
    }
  }
}

resource "null_resource" "example_3" {

  provisioner "remote-exec" {
    inline = ["sudo sh /home/ubuntu/script_3"]

# Establishes connection to be used by all provisioners (i.e. remote-exec)
    connection {
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("/home/azureuser/EC2.pem")}"
      host = "${aws_instance.app_server.public_ip}"
      port = 22
    }
  }
}

