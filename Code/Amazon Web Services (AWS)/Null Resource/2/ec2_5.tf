provider "aws" {
  region     = "eu-central-1"
  shared_credentials_files = ["/Users/rahulwagh/.aws/credentials"]
}

resource "aws_instance" "ec2_example" {
  ami           = "ami-0767046d1677be5a0"
  instance_type =  "t2-micro"
  tags = {
    Name = "Terraform EC2 "
  }
}

# This null_resource will be executed everytime because of id = time().
resource "null_resource" "null_resource_simple" {
  
  # Look carefully in the trigger we have assigned time() which we change value every time you run $terraform apply command.
  triggers = {
    id = time()
  }

  provisioner "local-exec" {
    command = "echo Hello World"
  }
} 
