# provider
provider "aws" {
  region = var.aws_region
}

# VPC
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "VPC"
  }
}

# Subnet_Public
resource "aws_subnet" "public" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-west-1a"
  tags = {
    Name = "Subnet_Public"
  }
}

# Subnet_Private
resource "aws_subnet" "private" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-west-1b"
  tags = {
    Name = "Subnet_Private"
  }
}

# Internet Gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "Internet Gateway"
  }
}

# Route Table 
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "RouteTable_Public"
  }
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "RouteTable_Private"
  }
}

# Route Table Associations
resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "private" {
  subnet_id      = aws_subnet.private.id
  route_table_id = aws_route_table.private.id
}

# Route
resource "aws_route" "public" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gw.id
  depends_on             = [aws_route_table.public]
}

# Security Group
resource "aws_security_group" "public" {
  name        = "public"
  description = "Allow traffic for webserver"
  vpc_id      = aws_vpc.main.id
  ingress {
    description = "Allow all traffic from everywhere"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    Name = "public"
  }
}

resource "aws_security_group" "private" {
  name        = "private"
  description = "Allow traffic for webserver"
  vpc_id      = aws_vpc.main.id
  ingress {
    description      = "Allow all traffic from everywhere"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    Name = "private"
  }
}

# Elastic Network Interface
resource "aws_network_interface" "public" {
  subnet_id       = aws_subnet.public.id
  private_ips     = ["10.0.1.7"]
  security_groups = [aws_security_group.public.id]
}

resource "aws_network_interface" "private" {
  subnet_id       = aws_subnet.private.id
  private_ips     = ["10.0.2.7"]
  security_groups = [aws_security_group.private.id]
}

# EC2
resource "aws_instance" "public" {
# ami           = "ami-005e54dee72cc1d00"
  ami = lookup(var.aws_amis, var.aws_region)
  instance_type = "t2.micro"
  network_interface {
    network_interface_id = aws_network_interface.public.id
    device_index         = 0
  }
  tags = var.tags
}

resource "aws_instance" "private" {
# ami           = "ami-0638cf52a5b22be19"
  ami = lookup(var.aws_amis, var.aws_region)
  instance_type = "t2.micro"
  network_interface {
    network_interface_id = aws_network_interface.private.id
    device_index         = 0
  }
  tags = {
    Name = "${var.name}-instance"
    env  = "Dev"
  }
}


#-------------> Bastion Host <-------------#
resource "aws_security_group" "bastion" {
  name        = "Bastion_Host"
  description = "Allow SSH traffic for Bastion Host"
  vpc_id      = aws_vpc.main.id
  ingress {
    description      = "Allow all traffic from everywhere"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  tags = {
    Name = "bastion"
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "bastion_host" {
  count = var.count_no
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name = "us-west-1"
  vpc_security_group_ids = [aws_security_group.bastion.id]
  subnet_id = aws_subnet.public.id
  associate_public_ip_address = true
}
