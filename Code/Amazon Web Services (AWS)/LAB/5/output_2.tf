output "private_instance" {
  description = "private instance details."
  value = {
    id = aws_instance.private.id
    instance_type = aws_instance.private.instance_type
    tag = aws_instance.private.tags
    public_ip = aws_instance.private.public_ip
    private_ip = aws_instance.private.private_ip
    volume_id_1 = aws_instance.private.root_block_device[0].volume_id
    volume_id_2 = aws_instance.private.root_block_device.0.volume_id
  }
}
