resource "aws_instance" "foo" {
  ami           = "ami-06195c6348fb0b567"
  instance_type = "t2.micro"

  network_interface {
    network_interface_id = aws_network_interface.foo.id
    device_index         = 0
  }

  tags = {
    "Name" = "PROD"
  }

}
