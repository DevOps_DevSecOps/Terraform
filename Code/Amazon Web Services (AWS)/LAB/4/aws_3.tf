resource "aws_key_pair" "key" {
  key_name   = "ssh"
  public_key = file("aws.pub")
}

resource "aws_instance" "app_server" {
  ami           = "ami-0638cf52a5b22be19"
  instance_type = "t2.micro"
  key_name = aws_key_pair.key.key_name
  vpc_security_group_ids = [aws_security_group.sg.id]

  root_block_device {
    volume_type = "gp2"
    volume_size = 18
  }
}

resource "aws_security_group" "sg" {
  name = "security group"
  vpc_id = "vpc-0ad84d2db9d3d8bbe"
  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

}
