sudo usermod -aG docker $USER

newgrp docker

sudo chmod 777 /var/run/docker.sock

docker run -d --name sonarqube -p 9000:9000 sonarqube:lts-community
