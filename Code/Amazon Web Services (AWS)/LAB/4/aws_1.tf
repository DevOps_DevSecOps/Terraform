terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.10.0"
    }
  }
}

provider "aws" {
  region  = "us-west-2"
}

resource "aws_instance" "app_server" {
  ami           = "ami-0638cf52a5b22be19"
  instance_type = "t2.micro"
  key_name      = "EC2"

# Establishes connection to be used by all provisioners (i.e. file/local-exec/remote-exec)
  connection {
    type     = "ssh"
    user     = "ubuntu"
    private_key = "${file("/home/azureuser/EC2.pem")}"
    host = "${aws_instance.app_server.public_ip}"
    port = 22
  }

# local-exec
  provisioner "local-exec" {
    command = "echo it is local-provisioner"
  }

}

# Null Resource
resource "null_resource" "example" {

  provisioner "local-exec" {
    command = "echo 'use provisioners in null, but just a demo ${aws_instance.app_server.public_ip}' >> ip"

    connection {
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("/home/azureuser/EC2.pem")}"
      host = "${aws_instance.app_server.public_ip}"
      port = 22
    }
  }

   provisioner "remote-exec" {
     inline = [
       "echo 'use provisioners in null, but just a demo ${aws_instance.app_server.id}' >> id"
     ]

    connection {
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("/home/azureuser/EC2.pem")}"
      host = "${aws_instance.app_server.public_ip}"
      port = 22
    }
  }

}

