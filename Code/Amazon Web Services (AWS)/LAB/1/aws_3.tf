terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.8.0"
    }
  }
}


provider "aws" {
  region  = "us-west-1"
}


variable "ClouD" {
  description = "name for all below resources"
  type = string
  default = "AWS"
}


# create default vpc if one does not exit
resource "aws_default_vpc" "default_vpc" {
  tags = {
    Name = "default vpc"
  }
}


# use data source to get all avalablility zones in region
data "aws_availability_zones" "available_zones" {}


# create default subnet if one does not exit
resource "aws_default_subnet" "default_subnet" {
  availability_zone = data.aws_availability_zones.available_zones.names[0]
  tags = {
    Name = "default subnet"
  }
}

resource "aws_subnet" "subnet" {
  vpc_id     = aws_default_vpc.default_vpc.id
  cidr_block = "172.31.52.0/28"
  availability_zone = "us-west-1a"
  tags = {
    Name = "Subnet_1 ${var.ClouD}"
  }
}


resource "aws_route_table_association" "RT" {
  subnet_id = aws_subnet.subnet.id
  route_table_id = "rtb-037f5633b6b207519"
}


resource "aws_network_interface" "nic" {
  subnet_id       = aws_subnet.subnet.id
  private_ips     = ["172.31.52.8"]
  security_groups = [aws_security_group.ec2_security_group.id]
  tags = {
    Name = "${var.ClouD} NIC"
  }
}


# create security group for the ec2 instance
resource "aws_security_group" "ec2_security_group" {
  name        = "ec2 security group"
  description = "allow access on port 22"
  vpc_id      = aws_default_vpc.default_vpc.id

  # allow access on port 22
  ingress {
    description = "ssh access"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Security Group"
  }
}


resource "aws_key_pair" "key" {
  key_name   = "ssh"
  public_key = file("aws.pub")
}


# use data source to get a registered amazon linux 2 ami
data "aws_ami" "amazon_linux_2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}


# launch the ec2 instance and install website
resource "aws_instance" "ec2_instance_1" {
  ami                    = "ami-07d2649d67dbe8900"
  instance_type          = "t2.micro"
  subnet_id              = aws_default_subnet.default_subnet.id
  security_groups        = ["sg-034caf5aae90e6732"]
  key_name               = "us-west-1"
  tags = {
    Name = "instance_1",
    Environment = "${var.ClouD}_Pre-Production"
  }
}

resource "aws_instance" "ec2_instance_2" {
  ami                    = data.aws_ami.amazon_linux_2.id
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.subnet.id
  vpc_security_group_ids = [aws_security_group.ec2_security_group.id]  
  key_name               = aws_key_pair.key.id
  tags = {
    Name = "instance_2"
    Environment = "Production"
  }
}

resource "aws_instance" "ec2_instance_3" {
  ami                    = "ami-07d2649d67dbe8900"
  instance_type          = "t2.micro"
  subnet_id              = "subnet-02d2209e4cb185a20"
  availability_zone      = "us-west-1b"
  security_groups        = toset(["sg-034caf5aae90e6732"])  
  tags = {
    Name = "${terraform.workspace}-instance_3"
  }
}

resource "aws_instance" "ec2_instance_4" {
  ami                    = data.aws_ami.amazon_linux_2.id
  instance_type          = "t2.micro"
  availability_zone      = "us-west-1a"  
  key_name               = aws_key_pair.key.key_name
  network_interface {
    network_interface_id = aws_network_interface.nic.id
    device_index         = 0
  }
  tags = {
    Name = "instance_4"
    workspace = terraform.workspace
  }
}
