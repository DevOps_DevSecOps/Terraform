vpc_cidr = "172.16.0.0/16"

vpc_tag = {
  "Name" = "vpc"
}

subnet_cidr = "172.16.10.0/24"

subnet_name = {
  "Name" = "subnet"
}

sg_name = {
  "Name" = "allow_tls"
}

instances = {
  instance_1 = {
    nic_name = { "Name" = "my_nic_1" }
    private_ips = ["172.16.10.101"]
    instance_Name = { "Name" = "PROD" }
    instance_ami = "ami-005e54dee72cc1d00"
    instance_type = "t2.micro"
  }  
  instance_2 = {
    nic_name = { "Name" = "my_nic_2" }
    private_ips = ["172.16.10.102"]
    instance_Name = { "Name" = "DEV" }
    instance_ami = "ami-005e54dee72cc1d00"
    instance_type = "t2.micro"
  }
}
