module "VPC" {
  source   = "./Modules/VPC"
  vpc_cidr = var.vpc_cidr
  vpc_tag  = var.vpc_tag
}

module "Subnet" {
  source      = "./Modules/Subnet"
  vpc_id      = module.VPC.vpc_id
  subnet_cidr = var.subnet_cidr
  subnet_name = var.subnet_name
}

module "SecurityGroup" {
  source  = "./Modules/SecurityGroup"
  vpc_id  = module.VPC.vpc_id
  sg_name = var.sg_name
}

module "NetworkInterface" {
  source      = "./Modules/NetworkInterface"
  subnet_id   = module.Subnet.subnet_id
  nic_name    = var.nic_name
  private_ips = var.private_ips
}

module "EC2_1" {
  source        = "./Modules/EC2"
  instance_Name = var.instance_Name
  instance_ami  = var.instance_ami
  instance_type = var.instance_type
  nic_id        = module.NetworkInterface.nic_id
}

module "EC2_2" {
  source        = "./Modules/EC2"
  providers = {
    aws = aws.east-2
  }
  instance_Name = var.instance_Name
  instance_ami  = var.instance_ami
  instance_type = var.instance_type
  nic_id        = module.NetworkInterface.nic_id
}
