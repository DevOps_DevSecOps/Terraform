module "VPC" {
  source = "./Modules/VPC"
}

module "Subnet" {
  source = "./Modules/Subnet"
}

module "SecurityGroup" {
  source = "./Modules/SecurityGroup"
}

module "NetworkInterface" {
  source = "./Modules/NetworkInterface"
}

module "EC2" {
  source = "./Modules/EC2"
}
