resource "aws_instance" "example" {
  ami = "ami-0767046d1677be5a0"
  instance_type = "t2.micro"
  
  # user_data attribute performing common tasks
  # 1. Create a new directory
  # 2. Install python
  # 3. Install nginx
  user_data     = <<-EOF
                    #!/bin/bash
                    
                    # Create a new directory
                    mkdir /opt/myapp
                    
                    # Install python
                    sudo apt install python3
                    
                    # Install nginx 
                    sudo apt install nginx
                  EOF
}
