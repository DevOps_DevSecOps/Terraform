provider "aws" {
  region     = "us-east-1"
# shared_credentials_files = ["/Users/rahulwagh/.aws/credentials"]
}


resource "aws_instance" "example" {
  ami = "ami-06195c6348fb0b567"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["sg-0cb45dab1790b3e7c"]
  key_name= "n.virginia"
  user_data     =  "${file("install_apache.sh")}"
}
