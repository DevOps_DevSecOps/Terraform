resource "aws_instance" "example" {
  ami           = "ami-033220e23cb64e5be"
  instance_type = (
    terraform.workspace == "default" ? "t2.medium" : "t2.micro"
  )
}


/*

So the terraform workspace is default, now run the 'terraform plan' OR 'terraform apply' then instance_type will be t2.medium

Created and Switched to workspace "production"
$ terraform workspace new production

List all existing workspaces, current workspace is indicated using an asterisk (*) marker
$ terraform workspace list
  default
* production

So the terraform workspace is production, now run the 'terraform plan' OR 'terraform apply' then instance_type will be t2.micro

*/
