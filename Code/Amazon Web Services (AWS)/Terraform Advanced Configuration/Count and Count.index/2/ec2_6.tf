resource "aws_instance" "demo" {
  count         = 3
  ami           = "ami-06195c6348fb0b567"
  instance_type = "t2.micro"
  user_data = <<-EOF
      #! /bin/bash
      sudo ssh-keygen -f ~/.ssh/DevOps
      sudo chmod 700 ~/.ssh
      sudo chmod 700 ~/.ssh/authorized_keys
    EOF
  tags = {
    Environment = "Demo-System_${count.index}"
    Name = "Demo-System_${count.index + 0}"
    name = "Demo-System_${count.index + 1}"
  }
}
