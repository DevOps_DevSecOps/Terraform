# It fetch the current system IP address to use in terraform
data "http" "myip" {
  url = "http://ipv4.icanhazip.com"
}

resource "aws_instance" "myEC2" {
  ami           = "ami-005e54dee72cc1d00"
  instance_type = lookup(var.instance_type, var.environment, "t2.nano")
  key_name = aws_key_pair.myKey.key_name
  security_groups = [aws_security_group.allow_ssh.name]
  
  tags = {
    Name = "AWS_EC2"
  }
}

resource "aws_key_pair" "myKey" {
  key_name   = "deployer-key"
  public_key = file("/home/USER_NAME/.ssh/id_rsa.pub")
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = "vpc-0720a5c2fc7a18218"

  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["${chomp(data.http.myip.body)}/32"]
  }
}

output "public_IP" {
  value = aws_instance.myEC2.public_IP
}
