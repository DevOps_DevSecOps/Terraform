variable "region" {
  default = "us-west-1"
}

variable "ec2_ami" {
  type = map
  default = {
    us-west-1 = "ami-0657605d763ac72a8"
    us-west-2 = "ami-07d9cf938edb0739b"
  }
}
