locals {
  aws_region = "us-west-2"
  name_prefix = "learning-terraform"
}

provider "aws" {
  region = local.aws_region
}

# Get latest AMI ID for AWS Linux
data "aws_ami" "aws_linux_latest" {
  most_recent = true
  filter {
    name = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
  owners = ["amazon"]
}

# Create EC2 instance using AWS Linux AMI ID
resource "aws_instance" "web01" {
  ami = data.aws_ami.aws_linux_latest.id
  instance_type = "t3.micro"
  tags = {
    Name = "${local.name_prefix}-web01"
  }
}
