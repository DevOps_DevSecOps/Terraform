provider "aws" {
  profile = "aws"
  region = "us-east-1"
}

variable "vpc_cidr" {
  default = [
    "10.0.0.0/16", 
    "10.0.0.0/24"
  ]
}

resource "aws_vpc" "main" {
  for_each = toset(var.vpc_cidr)
  cidr_block  =  each.value
}
