provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "my-machine" {
  ami = "ami-0a91cd140a1fc148a"
  for_each  = {
      key1 = "t2.micro"
      key2 = "t2.medium"
  }
  instance_type    = each.value 
  key_name         = each.key
  tags =  {
    Name = each.value 
  }
}
 
resource "aws_iam_user" "accounts" {
  for_each = toset( ["Account1", "Account2", "Account3", "Account4"] )
  name     = each.key
}
