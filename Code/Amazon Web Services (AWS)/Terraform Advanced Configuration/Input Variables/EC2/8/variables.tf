variable "ec2_tag" {
  description = "Tags to set for all resources"
  type        = map(string)
}

variable "ec2_count" {
  type = number
}

variable "ec2_instance_type" {
}
