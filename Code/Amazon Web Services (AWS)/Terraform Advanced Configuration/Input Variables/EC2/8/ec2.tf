resource "aws_instance" "app_server" {
  ami           = "ami-08d70e59c07c61a3a"
  instance_type = var.ec2_instance_type
  tags = var.ec2_tag
  count = var.ec2_count
}
