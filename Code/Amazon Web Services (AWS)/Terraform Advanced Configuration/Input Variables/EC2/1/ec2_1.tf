terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.8.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

variable "image_id" {
  default = "ami-04893cdb768d0f9ee"
}

resource "aws_instance" "example" {
  instance_type = "t2.micro"
  ami           = var.image_id
}
