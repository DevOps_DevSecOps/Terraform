provider "aws" {
  profile = "aws"
  region  = var.region
}

resource "aws_s3_bucket" "bucket" {
  bucket = var.s3_bucket_name
  acl    = var.acl

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}
