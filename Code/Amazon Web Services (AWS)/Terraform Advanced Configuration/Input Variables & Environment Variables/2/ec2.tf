terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.8.0"
    }
  }
}

provider "aws" {
  region = var.region
}

variable "region" {
  type = string
}

variable "image_id" { }

variable "instance_type" {}

resource "aws_instance" "example" {
  instance_type = var.instance_type
  ami           = var.image_id
}
