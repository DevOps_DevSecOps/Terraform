provider "aws" {
  profile = "aws"
  region  = var.region
}

resource "aws_s3_bucket" "bucket" {
  bucket = var.s3_bucket_name
  acl    = var.acl
  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}

resource "aws_s3_bucket_versioning" "versioning" {
  bucket = aws_s3_bucket.bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}
