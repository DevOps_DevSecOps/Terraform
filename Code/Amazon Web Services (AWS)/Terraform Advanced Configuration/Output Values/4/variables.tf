variable "ec2_region" {     
  type = string
  description = "specify the region"
}

variable "ec2_instance_type" {     
  description = "specify the instance type"
  type = string
}

variable "example_one_ami" {    
  description = "specify the AMI for first instance"  
  type = string
}

variable "example_two_ami" {    
  description = "specify the AMI for second instance"  
  type = string
}
