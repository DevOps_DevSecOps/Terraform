provider "aws" {
  region = "ap-south-1"
}

resource "aws_instance" "web" {
  ami           = "ami-0c1a7f89451184c8b"
  instance_type = "t2.micro"
  key_name = "Dev-UbuntuKey"
  tags= {
    Name = "terraform demo"
  }
}

output "public_dns"{
  value = aws_instance.web.public_dns
  description = "this is output values"
}
