provider "aws" {
  region = "us-east-1"
}

variable "vm_count" {
 type = number
 default = 1
}

resource "aws_instance" "my_web_servers" {
 ami = "ami-0d527b8c289b4af7f" //Ubuntu 20.04
 instance_type = "t2.micro"
 count = var.vm_count
}

output "instance_ip_addr" {
 value = aws_instance.my_web_servers[*].private_ip
}
