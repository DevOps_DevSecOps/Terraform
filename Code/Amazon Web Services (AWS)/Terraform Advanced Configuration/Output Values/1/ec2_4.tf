provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "outputs_example" {
  ami           = "ami-04893cdb768d0f9ee"
  instance_type = "t2.micro"
}

output "instance_private_ip" {
  value = aws_instance.outputs_example.private_ip
  description = "The PRIVATE IP of the EC2 Instance"
}

output "instance_public_ip" {
  value = aws_instance.outputs_example.public_ip
  description = "The PUBLIC IP of the EC2 Instance"
  sensitive   = true
}
