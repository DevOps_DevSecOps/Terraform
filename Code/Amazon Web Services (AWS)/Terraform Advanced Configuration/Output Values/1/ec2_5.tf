provider "aws" {
  region  = var.ec2_region
}

variable "ec2_region" {     
  type = string
  default = "us-east-1"
}

variable "ec2_instance_type" {     
  type = string
  default = "t2.micro"
}

variable "ec2_image" {     
  type = string
  default = "ami-04893cdb768d0f9ee"
}

resource "aws_instance" "example" {
  instance_type = var.ec2_instance_type
  ami           = var.ec2_image
}

output "instance_ip_addr" {
  value = aws_instance.example.public_ip
}
