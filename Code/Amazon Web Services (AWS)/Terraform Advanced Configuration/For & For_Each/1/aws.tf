provider "aws" {
  region = "us-east-1"
}

variable "cidrs" {
  type = set(string)
  default = [ "10.20.0.0/24", "10.20.1.0/24" ]
}

resource "aws_vpc" "main" {
  cidr_block = "10.20.0.0/16"
  tags = {
    "Name" = "JavaHome"
    "Environment" = "dev"  
  }
} 

resource "aws_subnet" "public" {
  for_each = var.cidrs
  cidr_block = each.value
  vpc_id = aws_vpc.main.id
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "main"
  }
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "example"
  }
}
