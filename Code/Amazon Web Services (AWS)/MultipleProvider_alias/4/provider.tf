provider "aws" {
  alias  = "west1"  
  region = "us-west-1"
}

provider "aws" {
  alias   = "west2"
  region  = "us-west-2"
  profile = "customprofile"
}
