output "VM_WEST_1" {
  value = module.server_1.server
}

output "VM_WEST_2" {
  value = module.server_2.server
}
