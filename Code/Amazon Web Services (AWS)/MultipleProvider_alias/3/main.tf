provider "aws" {
  region = "us-west-1"
}

module "server_1" {
  source        = "./EC2_Instance"
  region        = "us-west-1"
  server_name   = "WEST_1"
  ami           = "ami-07d2649d67dbe8900"  
  instance_type = "t2.micro"
  key_name      = "us-west-1"
}


provider "aws" {
  alias   = "west2"
  profile = "customprofile"
  region  = "us-west-2"
}

module "server_2" {
  source        = "./EC2_Instance"
  providers = {
    aws = aws.west2
  }
  region        = "us-west-2"
  server_name   = "WEST_2"
  ami           = "ami-00c257e12d6828491"  
  instance_type = "t2.micro"
  key_name      = "us-west-2"
}
