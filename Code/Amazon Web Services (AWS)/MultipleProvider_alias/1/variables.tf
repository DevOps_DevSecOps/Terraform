variable "region_west_1" {
  default     = "us-west-1"
  description = "AWS West 1 Region"
}

variable "region_west_2" {
  default     = "us-west-2"
  description = "AWS West 2 Region"
}

variable "ami_west_1" {
 default = "ami-00fd4a75e141e98d5"
 description = "Amazon Machine Image ID for Debian Server"
}

variable "ami_west_2" {
 default = "ami-05d38da78ce859165"
 description = "Amazon Machine Image ID for Ubuntu Server"
}

variable "type" {
 default = "t2.micro"
 description = "Size of VM"
}
