resource "aws_instance" "west-2" {
  count         = 2
  provider      = aws.aws_west_2
  ami           = var.ami_west_2
  instance_type = var.type

  tags = {
    name = "Demo System"
  }
}
