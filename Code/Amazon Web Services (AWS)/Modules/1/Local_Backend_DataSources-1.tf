terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.16.0"
    }
  }
}
 
provider "aws" {
  region = "us-east-1"
}
 
data "terraform_remote_state" "terraform_output" {
  backend = "local"
 
  config = {
    path = "../terraform-output/terraform.tfstate"
  }
}
 
resource "aws_subnet" "test_terraform_remote_state_subnet" {
  vpc_id            = data.terraform_remote_state.terraform_output.outputs.vpc_id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1b"
 
  tags = {
   Name = "test_terraform_remote_state_subnet"
  }
}
