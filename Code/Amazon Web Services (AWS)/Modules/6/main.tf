provider "aws" {
  region = "us-west-1"
}

module "web_server_instance" {
  source        = "./modules/web_server"
  region        = "us-west-1"
  ami           = "ami-033220e23cb64e5be"  
  instance_type = "t2.micro"
  server_name   = "web-instance"
}
