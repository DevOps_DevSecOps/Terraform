variable "region" {
  description = "AWS region"
}

variable "ami" {
  description = "AMI for the web server"
}

variable "instance_type" {
  description = "Instance type for the web server"
}

variable "server_name" {
  description = "Name tag for the web server"
}
