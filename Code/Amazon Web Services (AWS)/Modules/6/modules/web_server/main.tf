provider "aws" {
  region = var.region
}

resource "aws_instance" "web_server" {
  ami           = var.ami
  instance_type = var.instance_type
  key_name = "us-west-1"
  tags = {
    Name = var.server_name
  }
}
