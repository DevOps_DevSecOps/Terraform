module "Instance" {
  source = "./modules/EC2"

  vpc_id = module.Network.vpc
  subnet_public_id = module.Network.subnet_public
  subnet_private_id = module.Network.subnet_private
}


module "Network" {
  source = "./modules/Network"

  vpc_tag = var.vpc_tag
  az_public = var.az_public
  cidr_private = var.cidr_private
  destination_cidr_public = var.destination_cidr_public
  ig_tag = var.ig_tag
  az_private = var.az_private
}


module "S3" {
  source = "./modules/S3"

  bucket_name = "robin-example-2023-08-22"
  tag_s3_bucket = {
    Name = "creating a S3 bucket"
    environment = "PRODUCTION"
  }
}

