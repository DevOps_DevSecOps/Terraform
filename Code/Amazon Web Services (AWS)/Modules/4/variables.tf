variable "vpc_tag" {
  description = "tags values"
  type = map(string)
  default     = {
    project     = "project-Alpha",
    environment = "Dev"
  }
}

variable "az_public" {
  type = string
  default = "us-west-2a"
}

variable "cidr_private" {
  type = string
}

variable "az_private" { }

variable "destination_cidr_public" {
}

variable "ig_tag" {}

