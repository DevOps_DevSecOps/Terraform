resource "aws_network_interface" "public" {
  subnet_id       = var.subnet_public_id
  private_ips     = ["10.0.1.7"]
  security_groups = [aws_security_group.public.id]
}

resource "aws_network_interface" "private" {
  subnet_id       = var.subnet_private_id
  private_ips     = ["10.0.2.7"]
  security_groups = [aws_security_group.private.id]
}

