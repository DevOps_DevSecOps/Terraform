variable "bucket_name" {
  description = "Name of the s3 bucket. Must be unique."
  type        = string
}

variable "tag_s3_bucket" {
  description = "Tags to set on the bucket."
  type        = map(string)
  default     = {}
}


variable "status" {
  default = "Enabled"
}


variable "upload_s3_object" {
  description = "add tags to object"
  default = "objects.."
}

