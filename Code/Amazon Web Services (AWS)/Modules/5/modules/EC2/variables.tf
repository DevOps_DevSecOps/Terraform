variable "vpc_id" {
  description = "ID of the VPC where to create security group"
  type        = string
  default     = null
}

variable "subnet_public_id" {}

variable "subnet_private_id" { }

variable "private_count" {
  type = number
}

variable "public_ni_count" {
}

