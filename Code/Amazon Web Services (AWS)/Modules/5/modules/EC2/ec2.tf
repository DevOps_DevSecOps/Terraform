resource "aws_instance" "public" {
  count = 2
  ami           = "ami-005e54dee72cc1d00" 
  instance_type = "t3.small"
  subnet_id = var.subnet_public_id
  availability_zone = "us-west-2a"
/*
  network_interface {
    network_interface_id = "${element(aws_network_interface.public.*.id, count.index)}"
    device_index         = 0
  }
*/
  tags = {
    Name = "public_${count.index}"
  }
}

resource "aws_instance" "private" {
  count = var.private_count
  ami           = "ami-0638cf52a5b22be19" 
  instance_type = "t2.micro"
  network_interface {
    network_interface_id = aws_network_interface.private.id
    device_index         = 0
  }
  tags = {
    Name = "private-${count.index}"
  }
}

