resource "aws_instance" "web" {
  ami = "ami-0638cf52a5b22be19"
  instance_type = "t2.micro"
  tags = {
#   "Name" = "lifecycle"
    "Name" = "ignore_changes"
#   "Env" = "PROD"
    "Env" = "Dev"
  }
  lifecycle {
    ignore_changes = [
      tags,
    ]
  }
}

