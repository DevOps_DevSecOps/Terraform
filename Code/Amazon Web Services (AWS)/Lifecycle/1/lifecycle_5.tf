terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.8.0"
    }
  }
}

provider "aws" {
  region = "us-west-2"
}

resource "aws_instance" "web" {
# ami           = "ami-0638cf52a5b22be19"
  ami = "ami-0744bdf45532dfd8e"
  instance_type = "t2.micro"
  
  lifecycle {
    ignore_changes = [
      tags,
#     ami,
    ]
  }
}

