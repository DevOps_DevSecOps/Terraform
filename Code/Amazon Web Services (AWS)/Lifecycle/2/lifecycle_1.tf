# Create EC2 Instance
resource "aws_instance" "instance" {
  ami           = "ami-014d05e6b24240371" # Ubuntu
  instance_type = "t2.micro"
  availability_zone = "us-west-1b"

  tags = {
    Name = "demo1"
  }

  lifecycle {
    create_before_destroy = true
  }
}
