variable "ami" {
  type = string
  default = "ami-03a0c45ebc70f98ea"
  description = "Instance AMI"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "subnet_id" {
  default = "subnet-09c513f782t46"
}
