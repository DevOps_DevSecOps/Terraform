provider "aws" {
  region = "us-east-1"
}

resource "aws_vpc" "main" {
  cidr_block       = "10.20.0.0/16"
  instance_tenancy = "default"
  tags = {
    "Name" = "Java Home"
    "Location" = "Banglore"
    "Department" = "Training"
  }
}

terraform {
  backend "s3" {
    bucket = "jhc-iac-state"
    region = "us-east-1"
    key    = "terraform.tfstate"
  }
}
