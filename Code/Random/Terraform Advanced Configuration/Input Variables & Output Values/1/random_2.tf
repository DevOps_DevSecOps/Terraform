provider "random" {
  # Configuration options
}

variable "usersage" {
    type = map
    default = {
        gaurav = 20
        saurav = 19
    }
}

variable "username" {
  type = string
}

output "userage" {
    value = "my name is ${var.username} and my age is ${lookup(var.usersage, "${var.username}")}"
}


/*
lets run the terraform commands and check the output

$ terraform init

$ terraform plan -var username="gaurav"
Changes to Outputs:
  + userage = "my name is gaurav and my age is 20"
You can apply this plan to save these new output values to the Terraform state, without changing any real infrastructure.

$ terraform apply -var username="gaurav"
Changes to Outputs:
  + userage = "my name is gaurav and my age is 20"
You can apply this plan to save these new output values to the Terraform state, without changing any real infrastructure.
Do you want to perform these actions?
  Enter a value: yes
Apply complete! Resources: 0 added, 0 changed, 0 destroyed.
Outputs:
userage = "my name is gaurav and my age is 20"
*/


// LINK :- https://learning-ocean.com/tutorials/terraform/terraform-map/
