terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "6.14.1"
    }
  }
}


provider "google" {
  project = "smart-portfolio-441916-n8"
  region  = "us-central1"
  zone = "us-central1-a"
}


variable "environment" {
  description = "The environment for the deployment (e.g., dev, prod)"
  type        = string
}


resource "google_compute_network" "vpc_network" {
  name                    = "${var.environment}-vpc"
  auto_create_subnetworks  = false
}

resource "google_compute_subnetwork" "subnet" {
  name          = "subnet-${var.environment}"
  network       = google_compute_network.vpc_network.self_link
  ip_cidr_range = "172.16.11.0/28"
  region        = "us-central1"
}


terraform {
  backend "gcs" {
    bucket  = "terraformbackendstatefiles"
  }
}
