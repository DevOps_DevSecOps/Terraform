output "VM_1_name" {
  description = "Name of the VM 1"
  value       = module.server_1.name_of_server
}


output "VM_1_id" {
  description = "ID of the VM 1"
  value       = module.server_1.id_of_server
}


output "VM_2_name" {
  description = "Name of the VM 2"
  value       = module.server_2.name_of_server
}


output "VM_2_id" {
  description = "ID of the VM 2"
  value       = module.server_2.id_of_server
}
