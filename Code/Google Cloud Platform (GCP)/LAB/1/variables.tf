variable "Project_ID" {}

variable "zone" {
  description = "Name of the zone"
  default = "us-central1-a"
}
