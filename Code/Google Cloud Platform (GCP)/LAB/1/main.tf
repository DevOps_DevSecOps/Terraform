provider "google" {
  credentials = "./terraform-sa.json"
  project     = var.Project_ID
}

module "network" {
    source = "./modules/Network/"
    project_name = var.Project_ID
    vpc_name = "test-dev-vpc"
    subnet1_name = "test-pub-s1"
    subnet2_name = "test-pvt-s1"
    subnet1_cidr = "10.100.1.0/24"
    subnet2_cidr = "10.100.2.0/24"
    source_ranges = ["0.0.0.0/0"]
}

module "server_1" {
  source = "./modules/Servers/"
  server_name = "devops"
  zone_name = var.zone
  network_name = module.network.name_of_VPC
  subnet_name = module.network.id_of_SUBNET-1
}

module "server_2" {
  source = "./modules/Servers/"
  server_name = "devsecops"
  zone_name = "us-central1-f"
  network_name = module.network.name_of_VPC
  subnet_name = module.network.name_of_SUBNET-2
}
