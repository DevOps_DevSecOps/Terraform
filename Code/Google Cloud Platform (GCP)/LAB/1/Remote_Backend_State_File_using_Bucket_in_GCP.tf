# By default, the state files store in Local Backend where the terraform scripts been executed with terraform commands. 
# But it can also store the state file in Remote Backend using GCP Bucket. 

terraform {
  backend "gcs" {
    bucket  = "gcp_state_backend_tfstate"
    prefix  = "terraform/state"
  }
}
