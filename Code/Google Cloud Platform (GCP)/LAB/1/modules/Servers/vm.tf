resource "google_compute_instance" "VM" {
  name         = var.server_name
  machine_type = var.machine_type
  zone         = var.zone_name

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"     
    }
  }

  // Local SSD disk
  scratch_disk {
    interface = "NVME"
  }

  network_interface {
    network = var.network_name

    subnetwork = var.subnet_name

    access_config {
      // Ephemeral public IP
    }
  }

  metadata_startup_script = <<-EOF
    sudo apt update -y
    sudo apt install nginx -y
    echo "Hello World" > /file.txt
  EOF

}
