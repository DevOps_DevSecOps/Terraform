variable "project_name" { }

variable "vpc_name" {
  description = "Name of the VPC"
}

variable "subnet1_name" {
  description = "Name of the SUBNET-1"
}

variable "subnet1_cidr" {
  description = "CIDR range for SUBNET-1"
}

variable "subnet2_name" {
  description = "Name of the SUBNET-2"
}

variable "subnet2_cidr" {
  description = "CIDR range for subent-2"
}

variable "source_ranges" {
  description = "IP addresses that are allowed to initiate traffic towards a protected network"
}
