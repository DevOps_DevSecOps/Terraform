### VPC ###
resource "google_compute_network" "VPC" {
  project                 = var.project_name
  name                    = var.vpc_name
  auto_create_subnetworks = false
  mtu                     = 1460
}

### Subnet ###
resource "google_compute_subnetwork" "SUBNET-1" {
  name          = var.subnet1_name
  ip_cidr_range = var.subnet1_cidr
  region        = "us-central1"
  network       = google_compute_network.VPC.name
}

resource "google_compute_subnetwork" "SUBNET-2" {
  name          = var.subnet2_name 
  ip_cidr_range = var.subnet2_cidr
  region        = "us-central1"
  network       = google_compute_network.VPC.name
 
}

### Firewall ###
resource "google_compute_firewall" "FIREWALL" {
  name    = "firewall"
  network = google_compute_network.VPC.id

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "80", "8080", "3389"]
  }

  source_ranges = var.source_ranges
}
