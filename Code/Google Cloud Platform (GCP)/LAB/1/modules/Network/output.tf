output "name_of_VPC" {
    value = google_compute_network.VPC.name
}

output "id_of_VPC" {
    value = google_compute_network.VPC.id
}

output "name_of_SUBNET-1" {
    value = google_compute_subnetwork.SUBNET-1.name
}

output "id_of_SUBNET-1" {
    value = google_compute_subnetwork.SUBNET-1.id
}

output "name_of_SUBNET-2" {
    value = google_compute_subnetwork.SUBNET-2.name
}

output "id_of_SUBNET-2" {
    value = google_compute_subnetwork.SUBNET-2.id
}
