resource "google_storage_bucket" "BUCKET" {
  name          = "gcp_state_backend_tfstate"
  location      = "ASIA"
  storage_class = "ARCHIVE"
  versioning {
    enabled = true
  }
# force_destroy = true
}


resource "google_storage_bucket_object" "FOLDER" {
  name   = "terraform/state/"                                         # The folder will be named 'folder_name/'
  bucket = google_storage_bucket.BUCKET.name
  content = "Remote Backend State File using GCP Bucket"                                     
}
