output "Bucket"{
  value = google_storage_bucket.BUCKET.name
  sensitive = true
}


output "Bucket_Object-id"{
  value = google_storage_bucket_object.FOLDER.id
}


output "Bucket_Object-content_type"{
  value = google_storage_bucket_object.FOLDER.content_type
}


output "Bucket_Object-content"{
  value = nonsensitive(google_storage_bucket_object.FOLDER.content)
}
