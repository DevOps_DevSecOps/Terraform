variable "Project_ID"{ }

variable "subnet_2"{
  description = "A map of subnets to create"
  type        = map(string)
  default = {
    "subnet2-a" = "172.16.1.0/24"
    "subnet2-b" = "172.16.2.0/24"
    "subnet2-c" = "172.16.3.0/24"
  }
}

variable "subnet_3"{
  description = "A map of subnet names and CIDR blocks."
  type = map(object({
    name = string
    ip_cidr_range = string
  }))
  default = {
    network1 = { name = "subnet3-a", ip_cidr_range = "192.168.1.0/24" }
    network2 = { name = "subnet3-b", ip_cidr_range = "192.168.2.0/24" }
    network3 = { name = "subnet3-c", ip_cidr_range = "192.168.3.0/24" }
  }
}
