resource "google_compute_instance" "VM" {
  name         = var.server_name
  machine_type = "n2-standard-2"
  zone         = "us-central1-a"

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }

  // Local SSD disk
  scratch_disk {
    interface = "NVME"
  }

  network_interface {
    network = var.network_name

    subnetwork = var.subnet_name

    access_config {
      // Ephemeral public IP
    }
  }

  metadata = {
    startup-script = <<-EOT
      #!/bin/bash
      cp -r ./FOR_Arguments.py ~/.
      python3 python3 FOR_Arguments.py A B C D
    EOT
  }    

}
