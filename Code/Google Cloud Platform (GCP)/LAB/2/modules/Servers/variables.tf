variable "server_name" {
  description = "Name of the server"
}

variable "network_name" {
  description = "Name of the network"
}

variable "subnet_name" {
  description = "Name of the subnet"
}
