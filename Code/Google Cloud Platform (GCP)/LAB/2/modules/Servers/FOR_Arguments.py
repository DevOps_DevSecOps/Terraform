import sys

print(type(sys.argv))
print('The command line arguments are:')
for i in sys.argv:
    print(i)


'''
passing argument to command while execution
$ python3 FOR_Arguments.py A B C D
<class 'list'>
The command line arguments are:
FOR_Arguments.py
A
B
C
D
'''
