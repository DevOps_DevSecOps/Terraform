subnet_3 = {
  network4 = { name = "subnet3-d", ip_cidr_range = "192.168.4.0/24" }
  network5 = { name = "subnet3-e", ip_cidr_range = "192.168.5.0/24" }
  network6 = { name = "subnet3-f", ip_cidr_range = "192.168.6.0/24" }
}
