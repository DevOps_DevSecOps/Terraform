variable "Project_ID" {}


variable "Region" { }


variable "VPC_Name" {
}


variable "vpc-auto_create_subnetworks" {
  type = bool
}


variable "Subnet2_CIDR" {
  description = "range of internal ipv4 addresses for SUBNET_2"
  default = "172.16.0.0/24"
}


variable "Subnet3_CIDR" {
  default = "192.168.1.0/24"
}


variable "Firewall_Protocols" {
  description = "adding TCP protocols to firewall" 
}


variable "Firewall_Ports" {

}
