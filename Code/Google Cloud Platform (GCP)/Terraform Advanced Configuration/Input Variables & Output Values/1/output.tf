output "Name_of_VPC" {
  value = google_compute_network.VPC.name
}


output "ID_of_VPC" {
  value = google_compute_network.VPC.id
}


output "outputs_SUBNET_1" {
  value = {
    name = google_compute_subnetwork.SUBNET_1.name
    id = google_compute_subnetwork.SUBNET_1.id
  } 
}


output "All_outputs_listed_from_FIREWALL_1" {
    value = google_compute_firewall.FIREWALL_1
}
