resource "google_compute_firewall" "FIREWALL_1" {
  name    = "firewall-1"
  network = google_compute_network.VPC.id

  allow {
    protocol = var.Firewall_Protocols
    ports    = var.Firewall_Ports
  }

  source_ranges = ["0.0.0.0/0"]
}


resource "google_compute_firewall" "FIREWALL_2" {
  name    = "firewall-2"
  network = google_compute_network.VPC.id

  allow {
    protocol = var.Firewall_Protocols
    ports    = var.Firewall_Ports
  }

  source_tags = ["servers"]
  target_tags = ["vm"]
}
