resource "google_compute_network" "VPC" {
  project                 = var.Project_ID
  description             = "creating an VPC"
  name                    = var.VPC_Name
  auto_create_subnetworks = var.vpc-auto_create_subnetworks
  mtu                     = 1460
}
