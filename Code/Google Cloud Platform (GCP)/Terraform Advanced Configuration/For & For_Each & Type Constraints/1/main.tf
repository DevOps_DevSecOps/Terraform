provider "google" {
  project = "smart-portfolio-441916-n8"
  region  = "us-central1"                                       
}


resource "google_compute_network" "vpc" {
  name                    = "my-vpc"
  auto_create_subnetworks = false  
}

resource "google_compute_subnetwork" "subnet" {
  for_each = var.subnets

  name          = each.value.name
  network       = google_compute_network.vpc.id
  region        = each.value.region
  ip_cidr_range = each.value.ip_range

  depends_on = [google_compute_network.vpc]
}


output "subnet_ids" {
  value = { for s, subnet in google_compute_subnetwork.subnet : s => subnet.id }
}

output "subnet_details" {
  value = { for s, subnet in google_compute_subnetwork.subnet : s => {
    name       = subnet.name
    region     = subnet.region
    ip_range   = subnet.ip_cidr_range
    subnetwork = subnet.id
  }}
}
