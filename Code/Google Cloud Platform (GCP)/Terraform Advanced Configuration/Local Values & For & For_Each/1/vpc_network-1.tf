provider "google" {
  project = "smart-portfolio-441916-n8"
  region  = "us-central1"  
}


resource "google_compute_network" "vpc" {
  name                    = "my-vpc-network"
  auto_create_subnetworks = false
}


locals {
  subnets = {
    "subnet-a" = "10.0.1.0/24"
    "subnet-b" = "10.0.2.0/24"
    "subnet-c" = "10.0.3.0/24"
  }
}


resource "google_compute_subnetwork" "subnet" {
  for_each      = local.subnets
  name          = each.key
  ip_cidr_range = each.value
  region        = "us-central1"
  network       = google_compute_network.vpc.id
}


output "vpc_name" {
  value = google_compute_network.vpc
}

output "subnet_names" {
  value = { for s, subnet in google_compute_subnetwork.subnet : s => subnet.name }
}

output "subnet_ids" {
  value = [for s in google_compute_subnetwork.subnet : s.id]
}

output "subnet_attributes" {
  value = { for s, subnet in google_compute_subnetwork.subnet : s => {
    region     = subnet.region
    project_id = subnet.project
    cidr_range = subnet.ip_cidr_range
  }}   
}
