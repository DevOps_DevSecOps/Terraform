variable "project_id" {
  description = "The ID of the GCP project"
  type        = string
}

variable "region" {
  description = "The region to create resources in"
  type        = string
  default     = "us-central1"  
}

variable "vpc" {
  description = "The name of the VPC network"
  type        = string
  default     = "my-vpc-network"  
}

variable "subnets" {
  description = "A map of subnets to create"
  type        = map(string)
  default = {
    "subnet-1" = "10.0.1.0/24"
    "subnet-2" = "10.0.2.0/24"
    "subnet-3" = "10.0.3.0/24"
  }
}
