provider "google" {
  project = var.project_id  
  region  = var.region       
}


resource "google_compute_network" "vpc" {
  name                    = var.vpc
  auto_create_subnetworks = false
}


resource "google_compute_subnetwork" "subnets" {
  for_each = var.subnets
  name          = each.key
  ip_cidr_range = each.value
  region        = var.region
  network       = google_compute_network.vpc.id
  private_ip_google_access = true
}


output "subnet" {
  value = google_compute_subnetwork.subnets
}
