provider "google" {
  project = "smart-portfolio-441916-n8"
  region  = "us-central1"
}


variable "cidr_block" {
  description = "The CIDR block for the subnet"
  type        = string
  default     = "172.16.11.0/28"
}


resource "google_compute_network" "vpc_network" {
  name                    = "${terraform.workspace}-vpc"
  auto_create_subnetworks  = false
}


resource "google_compute_subnetwork" "subnet" {
  name          = "${terraform.workspace}-subnet"
  network       = google_compute_network.vpc_network.self_link
  ip_cidr_range = var.cidr_block
  region        = "us-central1"
}


output "vpc_name" {
  value = google_compute_network.vpc_network.name
}

output "subnet_name" {
  value = google_compute_subnetwork.subnet.name
}
