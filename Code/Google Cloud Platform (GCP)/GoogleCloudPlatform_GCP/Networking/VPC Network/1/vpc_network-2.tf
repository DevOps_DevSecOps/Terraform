resource "google_compute_network" "VPC" {
  project                 = "smart-portfolio-441916-n8"
  name                    = "vpc"
  auto_create_subnetworks = false
}


resource "google_compute_subnetwork" "SUBNET" {
  project                  = "smart-portfolio-441916-n8"
  name                     = "subnet"
  ip_cidr_range            = "10.1.0.0/24"
  region                   = "asia-southeast1"
  network                  = google_compute_network.VPC.id
  private_ip_google_access = true
}


resource "google_compute_firewall" "FIREWALL" {
  project = "smart-portfolio-441916-n8"
  name    = "firewall"
  network = google_compute_network.VPC.id

  allow {
    protocol = "tcp"
    ports    = ["80", "8080"]
  }

  source_ranges = ["0.0.0.0/0"]
}
