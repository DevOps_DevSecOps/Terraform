variable "environment" {
  description = "The environment for the deployment (e.g., dev, prod)"
  type        = string
}


resource "google_storage_bucket" "BUCKET" {
  project     = "smart-portfolio-441916-n8"
  name        = "gcp_state_backend_tfstate-${var.environment}"  
  location    = "us-central1"
}
