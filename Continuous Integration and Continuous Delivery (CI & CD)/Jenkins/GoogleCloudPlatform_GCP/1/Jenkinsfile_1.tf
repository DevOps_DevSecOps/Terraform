terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.74.0"
    }
  }
}

provider "google" {
  credentials = "${file("Jenkinsfile_1.json")}"
  project     = "smart-portfolio-441916-n8"
  region      = "us-central1"
  zone        = "us-central1-a"
}

resource "google_storage_bucket" "BUCKET" {
  name          = "gcp_state_backend_tfstate"
  location      = "us-central1"
}
