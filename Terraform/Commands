[terraform init]
● .terraform
It is a hidden directory/folder.
It uses to manage cached/store the provider plugins and modules.
It record which workspace is currently active other than "default" workspace.
It record the last known backend configuration.
● .terraform.lock.hcl
It is a hidden file and called as Dependency Lock File.
The inside of .terraform.lock.hcl file can see the information like provider name and provider plugin version. 
It use to lock provider plugin version to not keep on changing the provider plugin version.
In case we change the provider plugin version in .tf file, by using "terraform init -upgrade" command to upgrade provider plugin version.
It store provider plugin with different versions (old & new) in current location path ".terraform/providers/registry.terraform.io/hashicorp/aws/".
Inside of .terraform.lock.hcl file can see provider plugin new version that we mentioned in .tf file.

[terraform init -migrate-state]
Terraform detects that already have a state file locally and prompts you to migrate the state to the new Cloud Storage bucket. When prompted, enter yes.
After running this command, Terraform state is stored in the Cloud Storage bucket.
Terraform pulls the latest state from this bucket before running a command, and pushes the latest state to the bucket after running a command.

[terraform get]
It is used to download and update modules mentioned in the root module.
It is used to download it into the .terraform directory in current root directory.
The modules are downloaded into a .terraform subdirectory of the current working directory.
So don't commit .terraform directory to version control repository.
● -update
The modules that are already downloaded will be checked for updates and the updates will be downloaded if present.

[terraform refresh]
By running terraform refresh before any making manual changes to infrastructure resources, to ensure that Terraform state file is up-to-date and reflects the current state of infrastructure resources.
If any manual changes happened to infrastructure resources in cloud, executing terraform refresh command it update those changes in "terraform.tfstate" state file.
When creating a terraform plan OR running terraform apply, a refresh of the state of existing objects is automatically performed by Terraform (which is the default behavior).
If any manual changes happened to infrastructure resources in cloud without executing a terraform refresh command, instead of use executing a terraform plan OR terraform apply automatically it will refresh/refreshing the state file newly created with named as "terraform.tfstate" its up-to-date with those changes and older state file moved as "terraform.tfstate.backup" act like as previous version.
The changes will reflect to state file "terraform.tfstate", it stored in any type of Backends. 

[terraform import]
The terraform import command is used to import an existing infrastructure resources.
The command currently can only import one resource at a time. 
The terraform import command can only import resources into the state file "terraform.tfstate".
Before run the terraform import command, must manually write a resource configuration block for the resources.

[terraform taint & terraform untaint]
The terraform taint is uesd for particular resource marked as tainted.
Mostly the terraform taint command used for has become degraded or damaged or crashed or something went wrong of any type of resources.
So the taint command helps forcing it to be destroyed and recreate the new resource on the next terraform plan/apply command.
After destroyed and recreated the new any particular resource, then taint automatically removed/released along with the particular destroyed resource.
Suposse the particular resource is not destroyed means that approval is set be 'no', still the taint will be applicable for particular resource then manually remove the taint for particular resource by using terraform untaint command.
The terraform untaint is used to remove the taint for particular resource as tainted, so it unmarked as untainted.
This command is deprecated from v0.15.2 and later, Terraform recommends using "-replace" flag option with terraform plan/apply command. 
● -replace
A "-replace" flag that is used with terraform plan/apply command and is a suggested way to perform to destroyed and recreation of specific new resources.

[terraform state pull]
This command is used to manually download and output the state from remote state.
This command also works with local state.
This command downloads the state from its current location, upgrades the local copy to the latest state file version that is compatible with locally-installed Terraform, and outputs the raw format to stdout.

[terraform login & terraform logout]
The login & logout are used to communicate for Terraform Cloud via command line.
Terraform will assume want to log in to Terraform Cloud at "app.terraform.io".
Terraform prompt to ask an Token ID and to stored in plain text with file named called as 'credentials.tfrc.json' so the default located path of 'credentials.tfrc.json' file placed in "/home/USER_NAME/.terraform.d/credentials.tfrc.json".
